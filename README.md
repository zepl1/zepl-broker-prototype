# Zepl Broker Application
# Quickstart
* [Set up WebREPL on your board(s).](https://docs.micropython.org/en/latest/esp8266/tutorial/repl.html#webrepl-a-prompt-over-wifi)
* edit [cfg.json](doc/README.md#configuration-of-the-example-app)
* start zepl broker
```
./zepl_broker
```
* Start demo apps from other terminal
```
cd test_app/
python controller.py localhost
python log_listener.py localhost
```
