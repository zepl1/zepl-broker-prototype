#
# This file is part of Zepl Broker: https://gitlab.com/zepl1/zepl-broker
# (C)2020 Leonard Pollak <leonardp@tr-host.de>
#
# SPDX-License-Identifier:    AGPL-3

import asyncio
import zmq.asyncio

from pprint import pprint

class DeviceRunner:
    def __init__(self, ctx, uri_dev_in, uri_dev_out, cfg):
        """runner base class
        expects zmq.asyncio.Context from broker so inproc is possible
        """
        z_uri = f'inproc://{cfg["dev_ip"]}'

        self.dev_in = ctx.socket(zmq.SUB)
        self.dev_in.connect(uri_dev_in)
        if 'dev_id' in cfg:
            self.dev_in.subscribe(cfg['dev_id'].encode())
            print(f'device runner subscribed to: {cfg["dev_id"]}')
        self.dev_out = ctx.socket(zmq.PUB)
        self.dev_out.connect(uri_dev_out)
        # this one is technically device specific
        self.z_sock = ctx.socket(zmq.PAIR)
        self.z_sock.connect(z_uri)

        self.poller = zmq.asyncio.Poller()
        self.poller.register(self.z_sock, zmq.POLLIN)
        self.poller.register(self.dev_in, zmq.POLLIN)

        self._alive = False
        self.dev_id = None
        self.state  = 'wait_ready' # TODO others might be: 'active', 'idle', 'dead', ...

    def start(self):
        """start runner
        """
        self._alive = True
        self.runner_task = asyncio.ensure_future(self.runner())

    async def runner(self):
        """run poll loop
        one can monitor socket activity here
        """
        while self._alive:
            socks = dict(await self.poller.poll(100))

            if self.dev_in in socks.keys():
                await self.dev_in_handler()

            if self.z_sock in socks.keys():
                await self.dev_out_handler()

    async def dev_in_handler(self):
        """to be overridden by child
        UNTESTED
        """
        try:
            raw = await self.dev_in.recv_multipart()
            raw.pop(0) # strip topic
            print(f'DeviceRunner forwarding TO device {raw}')
            await self.z_sock.send_multipart(raw)
        except Exception as e:
            print(f'dev_in_handler failed: {e}')

    async def dev_out_handler(self):
        """to be overridden by child
        UNTESTED
        """
        try:
            raw = await self.z_sock.recv()
            print(f'DeviceRunner forwarding FROM device {raw}')
            msg = [self.dev_id].append(raw) # add dev_id as topic
            await self.dev_out.send_multipart(msg)
        except Exception as e:
            print(f'dev_out_handler failed: {e}')

    def stop(self):
        """stop runner
        """
        self._alive = False
        self.runner_task.cancel()
