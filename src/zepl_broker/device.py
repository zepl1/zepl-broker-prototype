#
# This file is part of Zepl Broker: https://gitlab.com/zepl1/zepl-broker
# (C)2020 Leonard Pollak <leonardp@tr-host.de>
#
# SPDX-License-Identifier:    AGPL-3

import zmq.asyncio

class ZeplDevice:
    def __init__(self,  ctx, uri_dev_in, uri_dev_out, cfg):
        """assemble zepl device class
        passing the context enables the use of inproc transport

        z_uri: device facing IO
        uri_dev_in: sub socket broker->device
        uri_dev_out: pub socker device->broker
        """

        for x in ['proto', 'dev_ip', 'dev_port','dev_pw']:
            if x not in cfg.keys():
                raise ValueError(f'you need to specify {x} in your config.')

        if cfg['proto'] == 'WebREPL':
            from .webrepl import WebReplDevice
            from zepl.protocols.webrepl import WebReplIo
            self.dev_runner = WebReplDevice(ctx, uri_dev_in, uri_dev_out, cfg)
            self.io_runner = WebReplIo(ctx=ctx, cfg=cfg)
            self.runners = [self.dev_runner, self.io_runner]
        else:
            raise NotImplementedError(f'Protocol ({proto}) not implemented')

        print('ZeplDevice initialized.')

    def start(self):
        """ start runners
        """
        for runner in self.runners:
            runner.start()

    def stop(self):
        """ stop runners
        """
        for runner in self.runners:
            runner.stop()
