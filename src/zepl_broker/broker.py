#
# This file is part of Zepl Broker: https://gitlab.com/zepl1/zepl-broker
# (C)2020 Leonard Pollak <leonardp@tr-host.de>
#
# SPDX-License-Identifier:    AGPL-3

import asyncio
import zmq.asyncio

from .device import ZeplDevice

class ZeplBroker():
    uri_dev_in = 'inproc://dev_in'
    uri_dev_out = 'inproc://dev_out'

    ctx = zmq.asyncio.Context()

    dev_out = ctx.socket(zmq.SUB)
    dev_out.bind(uri_dev_out)
    dev_out.subscribe(b'')

    dev_in = ctx.socket(zmq.PUB)
    dev_in.bind(uri_dev_in)

    log_out = ctx.socket(zmq.PUB)
    controller = ctx.socket(zmq.ROUTER)

    _alive = False

    def __init__(self, cfg=None, uri_log_out='tcp://0.0.0.0:9000', uri_server='tcp://0.0.0.0:9001'):
        self.loop = asyncio.get_event_loop()
        self.log_out.bind(uri_log_out)
        self.controller.bind(uri_server)
        self.dev_map = {}
        self.dev_cfg = cfg

    def start(self):
        self._alive = True
        # start initial worker threads
        for dev in self.dev_cfg.keys():
            self.dev_map[dev] = self.add_device(dev, self.dev_cfg[dev])
        self.broker_task = asyncio.ensure_future(self.broker())

    def add_device(self, dev, cfg):
        dd = {}
        device = ZeplDevice(self.ctx, self.uri_dev_in, self.uri_dev_out, cfg)
        device.start()
        device.cfg = cfg
        dd = { 'Z_DEV': device }
        return dd

    def stop_device(self, dev):
        self.dev_map[dev]['Z_DEV'].stop()

    def remove_device(self, dev):
        """collect garbage?
        """
        self.stop_device(dev)
        del self.dev_map[dev]

    async def broker(self):
        poller = zmq.asyncio.Poller()
        poller.register(self.dev_out, zmq.POLLIN)
        poller.register(self.controller, zmq.POLLIN)

        self._alive = True
        while self._alive:
            socks = dict(await poller.poll(100))

            if self.controller in socks.keys():
                try:
                    raw = await self.controller.recv_multipart()
                    raw[0], raw[1] = raw[1], raw[0]
                    await self.dev_in.send_multipart(raw)
                except Exception as e:
                    print(f'broker(controller) dropping msg: {raw} -- {e}')

            if self.dev_out in socks.keys():
                try:
                    topic, msg_type, prefix, msg = await self.dev_out.recv_multipart()
                except Exception as e:
                    print(f'ex in broker dev_out {e}')

                if msg_type == b'raw':
                        await self.log_out.send_multipart([topic, prefix, msg])
                elif msg_type == b'ctrl':
                        await self.controller.send_multipart([prefix, msg])
                else:
                    #print(f'broker(dev_out) dropping msg: {topic}{msg_type}{raw}')
                    pass

    def stop(self):
        self._alive = False
        devs = [d for d in self.dev_map]
        for ip in devs:
            self.remove_device(ip)
        self.broker_task.cancel()
        print('Terminated!')
