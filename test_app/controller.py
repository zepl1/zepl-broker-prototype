#! /usr/bin/env python3
#
# This file is part of Zepl Broker: https://gitlab.com/zepl1/zepl-broker
# (C)2020 Leonard Pollak <leonardp@tr-host.de>
#
# SPDX-License-Identifier:    AGPL-3
import zmq, sys
from time import sleep

host = sys.argv[1]
uri = f'tcp://{host}:9001'

c = zmq.Context()
b = c.socket(zmq.DEALER)
b.connect(uri)

def req_rep(dev_id, typ, arg):
    cmd = [dev_id, typ, arg]
    print('sending: ', cmd)
    b.send_multipart(cmd)
    resp = b.recv_multipart()
    print('received: ', resp)

dev_id = b'240ac444e2e4'

req_rep(dev_id, b'cmd', b'greetings')
req_rep(dev_id, b'cmd', b'sync')
req_rep(dev_id, b'run', b'3')
req_rep(dev_id, b'cmd', b'greetings')
