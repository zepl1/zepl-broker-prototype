#! /usr/bin/env python3
#
# This file is part of Zepl Broker: https://gitlab.com/zepl1/zepl-broker
# (C)2020 Leonard Pollak <leonardp@tr-host.de>
#
# SPDX-License-Identifier:    AGPL-3
import zmq, sys

host = sys.argv[1]
uri = f'tcp://{host}:9000'

c = zmq.Context()
l = c.socket(zmq.SUB)
l.connect(uri)
dev_id = b''
l.subscribe(dev_id)

while True:
    topic, msg_type, msg = l.recv_multipart()
    print(f'{msg_type} -- {topic} --  {msg}')
