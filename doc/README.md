# Short Story

## Configuration of MP Boards
if you have [set up WebREPL on your board(s)](https://docs.micropython.org/en/latest/esp8266/tutorial/repl.html#webrepl-a-prompt-over-wifi) (hint: use zepl to test it :)) you can then either:

* edit [wifi.json](../mpy/wifi.json) to include your wifi's name and password

__OR__

* edit [boot.py](../mpy/boot.py) and provide your own code for setting up the wifi

## Configuration of the Example App
```
{
	"dut1": {
		"dev_ip": "192.168.0.55",	# required: WebREPL IP Address
		"dev_port": 8266,		# required: WebREPL Port
		"proto": "WebREPL",		# required: Protocol Name
		"dev_pw": "asdf",		# required: WebREPL Password
		"sync_flist": [
			"app_a.py",
			"app_b.py",
			"main.py",
			"wifi_util.py",
			"wifi.json",
			"boot.py",
		],				# files for example app
		"init_sync": true,		# optional, Default: false -- uploads all files from 'sync_flist', overwriting on target
		"dev_id": "30aea47424bc",	# optional, Default: false -- speeds up the device initialization
		"init_run": "3"			# optional, Default: false -- see: Problem#1
	},
	"dut2": {
		...
	}
}
```
## ZeroMQ Model
#### Legend
* black arrows: flow of data
* orange: Hardware
* grey: coroutine
* boxes: logical blocks (e.g. classes)
* green: minimal example app

![](model.png)

# Loong Story

Read [STEP-2](https://gitlab.com/zepl1/docs/-/blob/master/STEP-2.md) and [STEP-3](https://gitlab.com/zepl1/docs/-/blob/master/STEP-3.md)
